import threading

from host import host


class MyParentThread(threading.Thread):

    hostsList = list()
    victimList = list()

    hostLock = threading.Lock()
    exitSign = 0

    def __init__(self, threadID, name, timeOut = 0):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.timeOut = timeOut
