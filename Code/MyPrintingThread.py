from printer import printf
from MyParentThread import MyParentThread
from scapy.all import *

class MyPrintingThread(MyParentThread):

    def __init__(self, threadID, name, timeOut):
        MyParentThread.__init__(self, threadID, name, timeOut)

    def run(self):
        printf("Starting " + self.name)
        self.process_data()
        printf("Exiting " + self.name)

    def process_data(self):
        while not MyPrintingThread.exitSign:

            MyPrintingThread.hostLock.acquire()
            printf ("%s  hosts is \n %s" % (self.name, '\n '.join(map(str, MyPrintingThread.hostsList))))
            MyPrintingThread.hostLock.release()

            time.sleep(self.timeOut)
