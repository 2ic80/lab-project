import datetime
import netaddr
from host import host
from printer import printf
from MyParentThread import MyParentThread
from scapy.all import *
from multiprocessing import Pool


class MyScanningThread(MyParentThread):

    def __init__(self, threadID, name, timeOut, printHostsFound = False, IPRange = "192.168.0.0/24", interface = None,
                 random = True, intensity = "normal", scanVictims = True, verbose = False):
        MyParentThread.__init__(self, threadID, name, timeOut)
        self.IPRange = IPRange
        self.interface = interface
        self.printHostsFound = printHostsFound
        self.random = random
        self.intensity = intensity
        self.scanVictims = scanVictims
        self.verbose = verbose

    def run(self):
        printf(str(datetime.datetime.now()) + " Starting " + self.name)
        self.process_data()
        printf(str(datetime.datetime.now()) + " Exiting " + self.name)

    def process_data(self):
        while not MyScanningThread.exitSign:

            startTime = datetime.datetime.now()
            scanResult = self.scanningParallel()

            endTime = datetime.datetime.now()
            duration = endTime - startTime
            try:
                MyScanningThread.hostLock.acquire()
                if (self.verbose):
                    printf ("%s  hosts is \n %s" % (self.name, '\n '.join(map(str, MyScanningThread.hostsList))))
                MyParentThread.hostsList = list()

                if (self.scanVictims):
                    MyParentThread.victimList = list()

                for h in scanResult:
                    if(not self.exists(h, MyScanningThread.hostsList )):
                        MyScanningThread.hostsList.append(h)

                    if(self.scanVictims):
                        if (not self.exists(h, MyScanningThread.victimList)):
                            MyScanningThread.victimList.append(h)
                if (self.verbose):
                    printf ("%s changed hosts to \n %s" % (self.name,  '\n '.join(map(str, MyScanningThread.hostsList))))
                    printf("Duration of scan: %s" % ( str(duration)))
                if(self.printHostsFound):
                    MyParentThread.exitSign = 1
                    printf("The following hosts where scanned: \n %s" % ('\n '.join(map(str, MyScanningThread.hostsList))))
                    break
            finally:
                MyScanningThread.hostLock.release()
            time.sleep(self.timeOut)

    def exists(self, host, list):
        for h in list:
            if(host.ip == h.ip):
                if(host.mac == h.mac):
                    return True
        return False



    def scanning(self):
        conf.verb = 0
        if(self.interface):
            ans, unans = srp(Ether(dst="ff:ff:ff:ff:ff:ff")/ARP(pdst=self.IPRange), timeout = 2,
                             iface=self.interface, inter=0.1)
        else:
            ans, unans = srp(Ether(dst="ff:ff:ff:ff:ff:ff") / ARP(pdst=self.IPRange), timeout=2,
                             inter=0.1)
        hosts = list()

        for snd,rcv in ans:
            #display the results
            ip = rcv.sprintf(r"%ARP.psrc%")
            mac = rcv.sprintf(r"%Ether.src% ")
            h = host(ip, mac)
            hosts.append(h)
        return hosts



    def scanningParallel(self):

        if(self.intensity == "slow"):
            p = Pool(16)
        if(self.intensity == "normal"):
            p = Pool(64)
        if (self.intensity == "intensive"):
            p = Pool(256)

        if(self.random):
            hosta = p.map(scanningOne, self.genRandom(), chunksize=1)
        else:
            hosta = p.map(scanningOne, self.gen(), chunksize=1)
        p.close()
        p.join()

        hosts = list()

        for hostList in hosta:
            for host in hostList:
                hosts.append(host)

        return hosts

    def gen(self):

        for h in netaddr.ip.iter_unique_ips(self.IPRange):
            yield str(h), self.interface

    def genRandom(self):
        hostList = list()
        for h in netaddr.ip.iter_unique_ips(self.IPRange):
            hostList.append(h)

        random.shuffle(hostList)
        printf(hostList)

        for h in hostList:
            yield str(h), self.interface


def scanningOne(input):
    ip, interface = input
    conf.verb = 0

    if(interface):
        ans, unans = srp(Ether(dst="ff:ff:ff:ff:ff:ff")/ARP(pdst=ip), timeout = 2,
                        iface=interface, inter=0.1)
    else:
        ans, unans = srp(Ether(dst="ff:ff:ff:ff:ff:ff") / ARP(pdst=ip), timeout=2,
                         inter=0.1)
    hosts = list()

    for snd,rcv in ans:
        #display the results
        ip = rcv.sprintf(r"%ARP.psrc%")
        mac = rcv.sprintf(r"%Ether.src% ")
        h = host(ip, mac)
        hosts.append(h)
    return hosts